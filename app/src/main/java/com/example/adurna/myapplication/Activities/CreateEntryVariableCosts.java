package com.example.adurna.myapplication.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.adurna.myapplication.Objects.Booking;
import com.example.adurna.myapplication.Objects.Category;
import com.example.adurna.myapplication.Objects.FixCosts;
import com.example.adurna.myapplication.R;
import com.example.adurna.myapplication.SqliteUtil;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.widget.AdapterView.OnItemSelectedListener;

public class CreateEntryVariableCosts extends AppCompatActivity{


    ArrayList<Category> categories;
    ArrayList<Booking> bookings;
    ArrayList<FixCosts> fixCosts;

    SqliteUtil util = SqliteUtil.getInstance(CreateEntryVariableCosts.this);


    private final String positiveColor = "#008000";
    private final String negativeColor = "#ff3f3f";

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        final SqliteUtil util = SqliteUtil.getInstance(this);
        categories = util.getDatabaseEntryCategory();
        bookings = util.getDatabaseEntryBooking();
        fixCosts = util.getDatabaseEntryFixCosts();

        final Boolean isPositive = getIntent().getBooleanExtra("positive", true);
        final Boolean isFixCost = getIntent().getBooleanExtra("fixCosts", false);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_entry);
        // control if it red or green entry
        ((TextInputEditText) findViewById(R.id.paymentET)).setBackgroundColor(Color.parseColor(isPositive ? positiveColor : negativeColor));

        ImageButton calc = findViewById(R.id.creat_entry_button);
        calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isFixCost){
                    makeAFixCostEntryInDB(isPositive);
                } else {
                    makeAVariableEntryInDB(isPositive);
                }

                Intent i = new Intent(CreateEntryVariableCosts.this, MainActivity.class);
                openHome();

            }
        });

        final Spinner spinner = (Spinner) findViewById(R.id.kategory_spinner);
        // Get data from DB

        // array Lists with all Objects
        ArrayList<String> categorieStrings = new ArrayList<>();

        for (Category category : categories) {
            categorieStrings.add(category.getName());
        }

//        DialogForCatagories dialog = new DialogForCatagories();
//        spinner.onClick( dialog, 1);

        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            //spinner.
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {

                if(arg2==spinner.getAdapter().getCount()-1){
                    openDialog();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }

        });


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categorieStrings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinner.setAdapter(adapter);

    }

    public void openHome() {
        Intent i = new Intent(this, MainActivity.class);
        if (i.resolveActivity(getPackageManager()) != null) {

            startActivity(i);
        }
    }

    public void makeAVariableEntryInDB(boolean isPositive){


        boolean wrongValueFlag = false;
        Spinner spinner = (Spinner) findViewById(R.id.kategory_spinner);

        EditText text = findViewById(R.id.details_for_db);
        String datailsText = text.getText().toString();
        EditText sumText = findViewById(R.id.paymentET);

        Booking booking = new Booking();
        if(bookings.size() < 1)
            booking.setId(1);
            else
            booking.setId(bookings.get(bookings.size()-1).getId()+1);

        CharSequence cs = sumText.getText().toString();

        Pattern mPattern = Pattern.compile("(0|[1-9]+[0-9]*)?(\\.[0-9]{0,2})?");
        Matcher matcher = mPattern.matcher(cs);
        if( !matcher.matches() && getFloatFrom(sumText)!=0.0 ){

            wrongValueFlag = true;

            CharSequence toastArlert = "Wrong value as Sum or empty";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(this, toastArlert, duration);
            toast.show();
        }

        String categoryName = spinner.getSelectedItem().toString();

        booking.setCategoryId(util.getCategoryIdByName(categoryName));
        booking.setDetails(datailsText);
        if(isPositive)
            booking.setSum(getFloatFrom(sumText));
        else
            booking.setSum((-getFloatFrom(sumText)));

        booking.setTimestamp(System.currentTimeMillis());

        if(!wrongValueFlag )
            util.setDatabaseEntryBooking(booking);

    }

    public void makeAFixCostEntryInDB(boolean isPositiv){

        boolean wrongValueFlag = false;

        Spinner spinner = (Spinner) findViewById(R.id.kategory_spinner);

        EditText text = (EditText)findViewById(R.id.details_for_db);
        String datailsText = text.getText().toString();
        EditText sumText = (EditText)findViewById(R.id.paymentET);

        FixCosts fixCost = new FixCosts();

        if(fixCosts.size() < 1)
            fixCost.setId(1);
        else
            fixCost.setId(bookings.get(bookings.size()-1).getId()+1);

        CharSequence cs = sumText.getText().toString();

        Pattern mPattern = Pattern.compile("(0|[1-9]+[0-9]*)?(\\.[0-9]{0,2})?");
        Matcher matcher = mPattern.matcher(cs);
        if( !matcher.matches() && getFloatFrom(sumText)!=0.0 ){

            wrongValueFlag = true;

            CharSequence toastArlert = "Wrong value as Sum or empty";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(this, toastArlert, duration);
            toast.show();
        }

        String categoryName = spinner.getSelectedItem().toString();

        fixCost.setCategoryId(util.getCategoryIdByName(categoryName));
        fixCost.setDetails(datailsText);
        if(isPositiv)
            fixCost.setSum(getFloatFrom(sumText));
            else
                fixCost.setSum((-getFloatFrom(sumText)));
        fixCost.setTimestamp(System.currentTimeMillis());

        if(!wrongValueFlag)
            util.setDatabaseEntryFixCosts(fixCost);
    }

    float getFloatFrom(EditText txt) {
        try {
            return NumberFormat.getInstance().parse(txt.getText().toString()).floatValue();
        } catch (ParseException e) {
            return 0.0f;
        }
    }

    public void openDialog() {
        Intent i = new Intent(this, DialogForCatagories.class);
        if (i.resolveActivity(getPackageManager()) != null) {

            startActivity(i);
        }
    }
}
