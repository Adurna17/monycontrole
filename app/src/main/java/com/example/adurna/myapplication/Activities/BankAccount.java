package com.example.adurna.myapplication.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.adurna.myapplication.CardView.BookingData;
import com.example.adurna.myapplication.CardView.BookingDataAdapter;
import com.example.adurna.myapplication.Objects.Booking;
import com.example.adurna.myapplication.Objects.Category;
import com.example.adurna.myapplication.Objects.FixCosts;
import com.example.adurna.myapplication.R;
import com.example.adurna.myapplication.SqliteUtil;

import java.util.ArrayList;

/**
 * A class from a activity
 * that will be used for all function for fix cost
 */
public class BankAccount extends AppCompatActivity {

    SqliteUtil util;

    ArrayList<Category> categories;
    ArrayList<Booking> bookings;
    ArrayList<FixCosts> fixCosts;

    ArrayList<BookingData> bookingsForCard=new ArrayList<>();
    RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // init the util variable
        util = SqliteUtil.getInstance(this.getApplicationContext());
        // create the lists of database tables
        categories = util.getDatabaseEntryCategory();
        bookings = util.getDatabaseEntryBooking();
        fixCosts = util.getDatabaseEntryFixCosts();

        Log.i("outLoop", "categories: [" + categories.size() + "] bookings: [" + bookings.size() + "] fixCost: [" + fixCosts.size() + "] ");

        setContentView(R.layout.activity_bank_account);
        // init the recycleView variable rv
        rv=findViewById(R.id.recycleViewBooking);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        // init the buttons
        ImageButton plus = findViewById(R.id.plus_btn_fix_cost);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // set the extras for the color and fix or booking costs
                Intent i = new Intent(BankAccount.this, CreateEntryVariableCosts.class);
                i.putExtra("positive", true);
                i.putExtra("fixCosts", true);
                if (i.resolveActivity(getPackageManager()) != null)
                    startActivity(i);
            }
        });

        ImageButton minus = findViewById(R.id.minus_btn_fix_cost);
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // set the extras for the color and fix or booking costs
                Intent i = new Intent(BankAccount.this, CreateEntryVariableCosts.class);
                i.putExtra("positive", false);
                i.putExtra("fixCosts", true);
                if (i.resolveActivity(getPackageManager()) != null)
                    startActivity(i);
            }
        });

        // the loop fill the recycler view
        for (FixCosts fixCosts : fixCosts) {
            Log.e("innerLoop", "Timestamp: [" + fixCosts.getTimestamp()
                    + "] Summe: [" + fixCosts.getSum() + "] Category ID: [" + fixCosts.getCategoryId() + "]");
            bookingsForCard.add(new BookingData(fixCosts.getTimestamp(), fixCosts.getSum() ,  util.getCategoryNameById(fixCosts.getCategoryId())));
        }

        rv.setAdapter(new BookingDataAdapter(bookingsForCard));
        // set the navigation
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.menu_fix_button);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        // Set the text with the sum of fix costs
        TextView sumOfBookings = findViewById(R.id.txt_activity_bank_value);
        sumOfBookings.setText(String.valueOf(util.getSumOfColumn("sum","fixedCosts")));
    }

    /**
     * Method to switch between the activities
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {


            switch (item.getItemId()) {
                case R.id.menu_buching_button:
                    openBooking();
                    return true;
                case R.id.menu_home_button:
                    openHome();
                    return true;
            }
            return false;
        }
    };

    /**
     * Method to go on the main activity
     */
    public void openHome(){
        Intent i = new Intent(this, MainActivity.class);
        if(i.resolveActivity(getPackageManager()) != null){

            startActivity(i);
        }
    }

    /**
     * Method to go on the booking activity
     */
    public void openBooking(){
        Intent i = new Intent(this, BookingActivity.class);
        if(i.resolveActivity(getPackageManager()) != null){

            startActivity(i);
        }
    }
}
