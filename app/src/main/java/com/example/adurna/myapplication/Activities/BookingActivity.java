package com.example.adurna.myapplication.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.adurna.myapplication.CardView.BookingData;
import com.example.adurna.myapplication.CardView.BookingDataAdapter;
import com.example.adurna.myapplication.Objects.Booking;
import com.example.adurna.myapplication.Objects.Category;
import com.example.adurna.myapplication.Objects.FixCosts;
import com.example.adurna.myapplication.R;
import com.example.adurna.myapplication.SqliteUtil;

import java.util.ArrayList;

/**
 * The Class of booking activity
 * here will created a recycle view
 * in that view will diplay all db entries from booking table
 */
public class BookingActivity extends AppCompatActivity {

    SqliteUtil util;
    ArrayList<FixCosts> fixCosts;
    ArrayList<Booking> bookings;
    ArrayList<Category> categories;


    ArrayList<BookingData> bookingsForCard =new ArrayList<>();
    RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // initial the db-connection
        SqliteUtil util = SqliteUtil.getInstance(this.getApplicationContext());
        // create all from every table a List
        categories = util.getDatabaseEntryCategory();
        bookings = util.getDatabaseEntryBooking();
        fixCosts = util.getDatabaseEntryFixCosts();
        Log.i("outLoop", "categories: [" + categories.size() + "] bookings: [" + bookings.size() + "] fixCost: [" + fixCosts.size() + "] ");
        /*
            here is the try to delet all entrys that are older then 30 days
            its not worked and its no more time to fix it
         */
        util.deletToOldEntry("timestamp","booking");

        setContentView(R.layout.activity_bookings);
        rv=findViewById(R.id.recycleViewBooking);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        // fill the recycle view
        for (Booking booking : bookings) {
//            util.getCategoryNameById(booking.getCategoryId())
            Log.e("innerLoop", "Timestamp: [" + booking.getTimestamp()
                    + "] Summe: [" + booking.getSum() + "] Category: [" + util.getCategoryNameById(booking.getCategoryId()) + "]");
                bookingsForCard.add(new BookingData(booking.getTimestamp(), booking.getSum() ,  util.getCategoryNameById(booking.getCategoryId())));
        }
        // set the data in the recycle view
        rv.setAdapter(new BookingDataAdapter(bookingsForCard));

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.menu_buching_button);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        // set the sum of all bookings
        TextView sumOfBookings = findViewById(R.id.txt_activity_booking_value);
        float sumOfBookingsFloat = util.getSumOfColumn("sum","booking");
        Log.i("MonyControll_DtaInfo", "onCreate: Sum of bookings: [" + sumOfBookingsFloat + "]" );
        sumOfBookings.setText(String.valueOf(sumOfBookingsFloat));

    }



    /**
     * the method to switch between Activities
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {


            switch (item.getItemId()) {
                case R.id.menu_home_button:
                    openHome();
                    return true;
                case R.id.menu_fix_button:
                    openFixCoast();
                    return true;
            }
            return false;
        }
    };

    /**
     * switch to main activity
     */
    public void openHome(){
        Intent i = new Intent(this, MainActivity.class);
        if(i.resolveActivity(getPackageManager()) != null){

            startActivity(i);
        }
    }
    /**
     * switch to fix cost activity
     */
    public void openFixCoast(){
        Intent i = new Intent(this, BankAccount.class);
        if(i.resolveActivity(getPackageManager()) != null){

            startActivity(i);
        }
    }
}
