package com.example.adurna.myapplication.CardView;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.adurna.myapplication.R;

import java.util.ArrayList;

/**
 * The booking adapter class
 */
public class BookingDataAdapter extends RecyclerView.Adapter {
    private ArrayList<BookingData> bookingList;

    /**
     * Constructor of BookingDataAdapter
     * @param buchingList
     */
    public BookingDataAdapter(ArrayList<BookingData> buchingList){
        this.bookingList = buchingList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardlayout,viewGroup,false);

        return new CardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((CardViewHolder)viewHolder).setBuchingData(bookingList.get(i));
    }

    @Override
    public int getItemCount() {
        return bookingList.size();
    }
}
