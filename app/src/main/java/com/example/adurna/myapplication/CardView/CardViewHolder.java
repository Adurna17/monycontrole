package com.example.adurna.myapplication.CardView;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.adurna.myapplication.R;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

class CardViewHolder extends RecyclerView.ViewHolder {
    TextView bookingReason;
    TextView booking;
    TextView timeStampe;


    public CardViewHolder(@NonNull View v) {
        super(v);
        booking =v.findViewById(R.id.txtview_booking);
        bookingReason =v.findViewById(R.id.txtview_buchingReason);
        timeStampe =v.findViewById(R.id.txtview_timestamp);

        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //TODO
                return false;
            }
        });

    }

    public void setBuchingData(BookingData buchingDtata) {
        String timeSTampeCorectFormat = new SimpleDateFormat("dd.MM.yyyy" ).format(new Timestamp((long)buchingDtata.getTimestamp()));

        if(buchingDtata.getValueOfBuching()<0) {
            booking.setTextColor(Color.RED);
            booking.setText("" + (buchingDtata.getValueOfBuching()*-1));
        }else{
            booking.setText("" + buchingDtata.getValueOfBuching());
        }
        bookingReason.setText(""+buchingDtata.getCategory());
        timeStampe.setText(""+ timeSTampeCorectFormat);
        // TODO hier der FarbwEchsel von negativen und positiven  Wwerten
    }

}
