package com.example.adurna.myapplication.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.adurna.myapplication.R;
import com.example.adurna.myapplication.SqliteUtil;
import com.github.lzyzsd.circleprogress.ArcProgress;

public class MainActivity extends AppCompatActivity {


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.menu_home_button);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        ImageButton plus = findViewById(R.id.plus_btn);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, CreateEntryVariableCosts.class);
                i.putExtra("positive", true);
                i.putExtra("fixCosts", false);
                if (i.resolveActivity(getPackageManager()) != null)
                    startActivity(i);
            }
        });

        ImageButton minus = findViewById(R.id.minus_btn);
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, CreateEntryVariableCosts.class);
                i.putExtra("positive", false);
                i.putExtra("fixCosts", false);
                if (i.resolveActivity(getPackageManager()) != null)
                    startActivity(i);
            }
        });

        ArcProgress progress = findViewById(R.id.arc_progress);
        SqliteUtil util = SqliteUtil.getInstance(this.getApplicationContext());

        float sumOfBookingsLong  = util.getSumPositiveOfColumn
                ("sum","booking") + util.getSumPositiveOfColumn("sum","fixedCosts");
        float wrastedBudgetLong= util.getSumNegativeOfColumn
                ("sum","booking") + util.getSumNegativeOfColumn("sum","fixedCosts");
        float calcedBudgetLong = (wrastedBudgetLong+sumOfBookingsLong);


        TextView sumOfBookings = findViewById(R.id.txt_activity_main_start_budget);
        sumOfBookings.setText(String.valueOf(sumOfBookingsLong));

        TextView calcedBudget = findViewById(R.id.txt_activity_main_calced_budget);
        calcedBudget.setText(String.valueOf(calcedBudgetLong));

        TextView wrastedBudget = findViewById(R.id.txt_activity_main_wrast_budget);
        wrastedBudget.setTextColor(Color.RED);
        wrastedBudget.setText(String.valueOf(wrastedBudgetLong));

        progress.setProgress((int)(((wrastedBudgetLong*-1) / sumOfBookingsLong)*100));

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {


            switch (item.getItemId()) {
                case R.id.menu_buching_button:
                    openBuching();
                    return true;
                case R.id.menu_home_button:
//                    openHome();
                    return true;
                case R.id.menu_fix_button:
                    openFixCoast();
                    return true;
            }
            return false;
        }
    };

    public void openBuching() {
        Intent i = new Intent(this, BookingActivity.class);
        if (i.resolveActivity(getPackageManager()) != null) {

            startActivity(i);
        }
    }

    public void openFixCoast() {
        Intent i = new Intent(this, BankAccount.class);
        if (i.resolveActivity(getPackageManager()) != null) {

            startActivity(i);
        }
    }

}
