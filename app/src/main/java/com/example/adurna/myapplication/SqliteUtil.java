package com.example.adurna.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.adurna.myapplication.Objects.Booking;
import com.example.adurna.myapplication.Objects.Category;
import com.example.adurna.myapplication.Objects.FixCosts;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * This Class is for create and control the Database
 *
 * @author Benedikt Dahms
 * @version 1.0
 */
public class SqliteUtil extends SQLiteOpenHelper {

    private static SqliteUtil sqInstance;
    private ArrayList<Category> allCategories;
    private ArrayList<Booking> allBookings;
    private ArrayList<FixCosts> allFixCosts;
    private SQLiteDatabase myDB;
    private Context context;

    // Name of database
    private static final String DATABASE_NAME = "Monycontrole.db";
    // Name of Tables
    private static final String DATABASE_NAME_OF_TABLE_BOOKING = "booking";
    private static final String DATABASE_NAME_OF_TABLE_CATEGORY = "category";
    private static final String DATABASE_NAME_OF_TABLE_FIXED_COSTS = "fixedCosts";
    // Name of Columns
    private static final String DATABASE_COLUMN_ID = "id";
    private static final String DATABASE_COLUMN_NAME_OF_CATEGORY = "name";
    private static final String DATABASE_COLUMN_CATEGORY_ID = "categoryId";
    private static final String DATABASE_COLUMN_SUM = "sum";
    private static final String DATABASE_COLUMN_DETAIL = "detail";
    private static final String DATABASE_COLUMN_TIMESTAMP = "timestamp";

    /**
     * Private constructor for the singleton pattern
     *
     * @param context The context, on which the util should be created
     */
    private SqliteUtil(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.context = context;
    }

    /**
     * Getter for the current instance of the SqlieUtil
     *
     * @param context The context, on which the util should be created
     * @return The current SqlieUtil instance
     */
    public static SqliteUtil getInstance(Context context) {
        if (sqInstance == null)
            sqInstance = new SqliteUtil(context);
        return sqInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Create Database if it is not exist
        // Create the table Category
        db.execSQL("CREATE TABLE IF NOT EXISTS "
                + DATABASE_NAME_OF_TABLE_CATEGORY
                + " (" + DATABASE_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DATABASE_COLUMN_NAME_OF_CATEGORY + " TEXT )");

        // Create the table Bookings
        db.execSQL("CREATE TABLE IF NOT EXISTS "
                + DATABASE_NAME_OF_TABLE_BOOKING + " ("
                + DATABASE_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DATABASE_COLUMN_CATEGORY_ID + " INTEGER, "
                + DATABASE_COLUMN_SUM + " REAL, "
                + DATABASE_COLUMN_DETAIL + " TEXT, "
                + DATABASE_COLUMN_TIMESTAMP + " INTEGER)");

        // Create the table Fix costs
        db.execSQL("CREATE TABLE IF NOT EXISTS "
                + DATABASE_NAME_OF_TABLE_FIXED_COSTS + " ("
                + DATABASE_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DATABASE_COLUMN_CATEGORY_ID + " INTEGER, "
                + DATABASE_COLUMN_SUM + " REAL, "
                + DATABASE_COLUMN_DETAIL + " TEXT, "
                + DATABASE_COLUMN_TIMESTAMP + " INTEGER)");

        initDB();
    }

    /**
     * Initial the DB with default categories
     */
    public void initDB() {
        setDatabaseEntryCategory("Add");
        setDatabaseEntryCategory("Salary");
        setDatabaseEntryCategory("General");
        setDatabaseEntryCategory("Eating");
        setDatabaseEntryCategory("Hobby");
        setDatabaseEntryCategory("Sports");
        setDatabaseEntryCategory("Gift");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //make a hard upgrade
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME_OF_TABLE_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME_OF_TABLE_BOOKING);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME_OF_TABLE_FIXED_COSTS);
        onCreate(db);
    }


    /**
     * Set a category to the DB
     *
     * @param nameOfCategory here u must set the String value
     */
    public void setDatabaseEntryCategory(String nameOfCategory) {
        // init db
        myDB = this.getWritableDatabase();
        // The value that get all params for db
        ContentValues cv = new ContentValues();
        cv.put(DATABASE_COLUMN_ID, (getMaxID(DATABASE_NAME_OF_TABLE_CATEGORY) + 1));
        cv.put(DATABASE_COLUMN_NAME_OF_CATEGORY, nameOfCategory);


        Log.i("MonyControll_DtaInfo", "setDatabaseEntryCategory/ ID: [" + (getMaxID(DATABASE_NAME_OF_TABLE_CATEGORY) + 1) +
                "] nameOfCategory: [" + nameOfCategory + "]");
        myDB.insert(DATABASE_NAME_OF_TABLE_CATEGORY, null, cv);
    }

    /**
     * Set a normal booking in db
     *
     * @param booking is a booking object
     */
    public void setDatabaseEntryBooking(Booking booking) {
//      init db
        myDB = this.getWritableDatabase();
        // The value that get all params for db
        ContentValues cv = new ContentValues();
        cv.put(DATABASE_COLUMN_ID, (getMaxID(DATABASE_NAME_OF_TABLE_BOOKING) + 1));
        cv.put(DATABASE_COLUMN_CATEGORY_ID, booking.getCategoryId());
        cv.put(DATABASE_COLUMN_SUM, booking.getSum());
        cv.put(DATABASE_COLUMN_DETAIL, booking.getDetails());
        cv.put(DATABASE_COLUMN_TIMESTAMP, booking.getTimestamp());

        Log.i("MonyControll_DtaInfo", "Make the Entry Booking: " +
                "id [" + (getMaxID(DATABASE_NAME_OF_TABLE_BOOKING) + 1) + "], Details [" +
                booking.getDetails() + "], CategoryID [" + booking.getCategoryId() + "], Sum [" +
                booking.getSum() + "], timestamp [" + booking.getTimestamp() + "]");

        myDB.insert(DATABASE_NAME_OF_TABLE_BOOKING, null, cv);
    }

    /**
     * set a fix costs booking, that will be permanent
     *
     * @param fixCosts is a fixcost object
     */
    public void setDatabaseEntryFixCosts(FixCosts fixCosts) {
//      init db
        myDB = this.getWritableDatabase();
        // The value that get all params for db
        ContentValues cv = new ContentValues();

        cv.put(DATABASE_COLUMN_ID, (getMaxID(DATABASE_NAME_OF_TABLE_FIXED_COSTS) + 1));
        cv.put(DATABASE_COLUMN_CATEGORY_ID, fixCosts.getCategoryId());
        cv.put(DATABASE_COLUMN_SUM, fixCosts.getSum());
        cv.put(DATABASE_COLUMN_DETAIL, fixCosts.getDetails());
        cv.put(DATABASE_COLUMN_TIMESTAMP, fixCosts.getTimestamp());

        Log.i("MonyControll_DtaInfo", "Make the Entry Booking: " +
                "id [" + (getMaxID(DATABASE_NAME_OF_TABLE_FIXED_COSTS) + 1) + "], Details [" +
                fixCosts.getDetails() + "], CategoryID [" + fixCosts.getCategoryId() + "], Sum [" +
                fixCosts.getSum() + "], timestamp [" + fixCosts.getTimestamp() + "]");

        myDB.insert(DATABASE_NAME_OF_TABLE_FIXED_COSTS, null, cv);

    }

    /**
     * get all db entrys that is from type category
     *
     * @return listOfCategories
     */
    public ArrayList<Category> getDatabaseEntryCategory() {
//      init db

        myDB = this.getReadableDatabase();
        //Get cursor for the values
        Cursor cursor = myDB.rawQuery("SELECT *  FROM " + DATABASE_NAME_OF_TABLE_CATEGORY + " ORDER BY " + DATABASE_COLUMN_ID + " DESC", null);
        allCategories = new ArrayList<>();

        // if is needed because its no other way to init the cursor
        if (cursor.moveToFirst()) {
            // looping through all rows and adding to list
            do {
                Category obj = new Category();

                obj.setId(cursor.getInt(0));
                obj.setName(cursor.getString(1));

                allCategories.add(obj);
            } while (cursor.moveToNext());
        }
        return allCategories;
    }

    /**
     * get all db entrys that is from type category
     *
     * @return List of bookings
     */
    public ArrayList<Booking> getDatabaseEntryBooking() {
//      init db
        myDB = this.getReadableDatabase();
        //Get cursor for the values
        Cursor cursor = myDB.rawQuery("SELECT *  FROM " + DATABASE_NAME_OF_TABLE_BOOKING + " ORDER BY " + DATABASE_COLUMN_ID + " DESC", null);
        allBookings = new ArrayList<>();

        // if is needed because its no other way to init the cursor
        if (cursor.moveToFirst()) {
            // looping through all rows and adding to list
            do {
                Booking obj = new Booking();
                //only one column
                obj.setId(cursor.getInt(0));
                obj.setCategoryId(cursor.getInt(1));
                obj.setSum(cursor.getFloat(2));
                obj.setDetails(cursor.getString(3));
                obj.setTimestamp(cursor.getLong(4));

                Log.i("MonyControll_DtaInfo", "Call the Entrys of Booking: " +
                        "id [" + obj.getId() + "], Details [" +
                        obj.getDetails() + "], CategoryID [" + obj.getCategoryId() + "], Sum [" +
                        obj.getSum() + "], timestamp [" + obj.getTimestamp() + "]");

                allBookings.add(obj);
            } while (cursor.moveToNext());
        }
        return allBookings;

    }

    /**
     * get all db entrys that is from type fixcosts
     *
     * @return List of fixcosts
     */
    public ArrayList<FixCosts> getDatabaseEntryFixCosts() {
//      init db
        myDB = this.getReadableDatabase();
        //Get cursor for the values
        Cursor cursor = myDB.rawQuery("SELECT *  FROM " + DATABASE_NAME_OF_TABLE_FIXED_COSTS + " ORDER BY " + DATABASE_COLUMN_ID + " DESC", null);
        allFixCosts = new ArrayList<>();

        // if is needed because its no other way to init the cursor
        if (cursor.moveToFirst()) {
            // looping through all rows and adding to list
            do {
                FixCosts obj = new FixCosts();
                obj.setId(cursor.getInt(0));
                obj.setCategoryId(cursor.getInt(1));
                obj.setSum(cursor.getFloat(2));
                obj.setDetails(cursor.getString(3));
                obj.setTimestamp(cursor.getLong(4));

                Log.i("MonyControll_DtaInfo", "Call the Entrys of Booking: " +
                        "id [" + obj.getId() + "], Details [" +
                        obj.getDetails() + "], CategoryID [" + obj.getCategoryId() + "], Sum [" +
                        obj.getSum() + "], timestamp [" + obj.getTimestamp() + "]");

                allFixCosts.add(obj);
            } while (cursor.moveToNext());
        }
        return allFixCosts;

    }

    /**
     * get the max id of a table
     *
     * @param tableName the name of table is needed
     * @return the max id of table
     */
    public int getMaxID(String tableName) {
        int mx = -1;
        //      init db
        myDB = this.getReadableDatabase();

        Cursor cursor = myDB.rawQuery("SELECT MAX(id) FROM " + tableName, null);
        if (cursor != null)
            // if is needed because its no other way to init the cursor
            if (cursor.moveToFirst()) {

                mx = cursor.getInt(0);

            }
        return mx;
    }

    /**
     * Get the right Category name by ID
     *
     * @param id set the id of category table id
     * @return return a String in this stay the Name of category
     */
    public String getCategoryNameById(int id) {
//      init db
        myDB = this.getReadableDatabase();
        String categoryName;
        Cursor cursor;
        cursor = myDB.rawQuery("select * from " + DATABASE_NAME_OF_TABLE_CATEGORY + " where "
                + DATABASE_COLUMN_ID + "='" + id + "'", null);

        // if is needed because its no other way to init the cursor
        if (cursor.moveToFirst()) {
            categoryName = cursor.getString(1);
        } else
            categoryName = "";

        return categoryName;
    }

    /**
     * Find the ID by a name of Category
     *
     * @param name of category
     * @return the ID of inserted category name
     */
    public int getCategoryIdByName(String name) {

        Cursor cursor;
        int categoryId = 0;
//      init db
        myDB = this.getReadableDatabase();
        cursor = myDB.rawQuery("select * from " + DATABASE_NAME_OF_TABLE_CATEGORY + " where "
                + DATABASE_COLUMN_NAME_OF_CATEGORY + "='" + name + "'", null);

        // if is needed because its no other way to init the cursor
        if (cursor.moveToFirst()) {
            categoryId = cursor.getInt(0);
        } else
            categoryId = -1;

        return categoryId;
    }

    /**
     * sum all values of a column
     *
     * @param column thats the name of the column
     * @param table  thats the name of the table
     * @return the result of summation
     */
    public float getSumOfColumn(String column, String table) {
//      init db
        myDB = this.getReadableDatabase();
        Cursor cur = myDB.rawQuery("SELECT SUM(" + column + ") FROM " + table, null);

        // if is needed because its no other way to init the cursor
        if (cur.moveToFirst()) {
            return cur.getInt(0);
        }
        return 0;
    }

    /**
     * get sum of all column entries that are > 0
     *
     * @param column name of column
     * @param table  name of table
     * @return sum of all positives values
     */
    public float getSumPositiveOfColumn(String column, String table) {
//      init db
        myDB = this.getReadableDatabase();

        Cursor cur = myDB.rawQuery("SELECT SUM(case when " + column + ">0 then " + column + " ELSE 0 END) FROM " + table, null);

        // if is needed because its no other way to init the cursor
        if (cur.moveToFirst()) {
            return cur.getInt(0);
        }
        return 0;
    }

    /**
     * get sum of all column entries that are < 0
     *
     * @param column name of column
     * @param table  name of table
     * @return sum of all negative values
     */
    public float getSumNegativeOfColumn(String column, String table) {
//      init db
        myDB = this.getReadableDatabase();

        Cursor cur = myDB.rawQuery("SELECT SUM(case when " + column + "<0 then " + column + " ELSE 0 END) FROM " + table, null);

        // if is needed because its no other way to init the cursor
        if (cur.moveToFirst()) {
            return cur.getInt(0);
        }
        return 0;
    }

    /**
     * Method not finished
     * This method delete all bookings that are older then 30 days (one month)
     *
     * @param column the name of the column
     * @param table  the name of table
     */
    public void deletToOldEntry(String column, String table) {

        long oneMonth = 2592000000L;
//      init db
        myDB = this.getReadableDatabase();

        Cursor cursor = myDB.rawQuery("SELECT *  FROM " + DATABASE_NAME_OF_TABLE_BOOKING + " ORDER BY " + DATABASE_COLUMN_TIMESTAMP + " DESC", null);
        boolean breakBoolean;
        // if is needed because its no other way to init the cursor
        if (cursor.moveToFirst()) {
            do {
                breakBoolean = true;
                FixCosts obj = new FixCosts();
                obj.setId(cursor.getInt(0));
                obj.setTimestamp(cursor.getLong(4));

                if (obj.getTimestamp() < (System.currentTimeMillis() - oneMonth)) {
                    myDB.delete(table, column + "=" + obj.getId(), null);
                    breakBoolean = false;
                }
            } while (cursor.moveToNext() && !breakBoolean);
        }


        long timeMinusAMonth = (System.currentTimeMillis() - oneMonth);
        String timeSTampeCorectFormat = new SimpleDateFormat("dd.MM.yyyy").format(new Timestamp(timeMinusAMonth));
        Log.i("MonyControll_DtaInfo", "DELETE FROM " + table + " WHERE " + column + " < " + (System.currentTimeMillis() - oneMonth) + " in Time: " + timeSTampeCorectFormat);

    }
    //TODO 2592000000 sind 30Tage
}
