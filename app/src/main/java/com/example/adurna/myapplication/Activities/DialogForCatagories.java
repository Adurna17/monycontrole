package com.example.adurna.myapplication.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.adurna.myapplication.R;
import com.example.adurna.myapplication.SqliteUtil;

/**
 * That is a activity for get a new Category name
 */
public class DialogForCatagories extends Activity implements View.OnClickListener{


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);
        Button but;

        but = new Button(this);
        but.findViewById(R.id.btn_creat_new_category);
    }
    /**
     * on click is for the Button to create a new category
     *
     * @param savedInstanceState
     */
    @Override
    public void onClick(View v){
        String categoryEntryText;
        EditText text = findViewById(R.id.get_text_new_category);
        categoryEntryText = text.getText().toString();
        SqliteUtil util = SqliteUtil.getInstance(DialogForCatagories.this);
        util.setDatabaseEntryCategory(categoryEntryText);
        openCreateEntry();
    }

    public void openCreateEntry(){
        Intent i = new Intent(this, CreateEntryVariableCosts.class);
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivity(i);
        }
    }
}
