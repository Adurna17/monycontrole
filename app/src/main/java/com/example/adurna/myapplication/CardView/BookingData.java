package com.example.adurna.myapplication.CardView;

/**
 * class to create a BookingData object
 */
public class BookingData {

    String ctategory;
    float valueOfBuching;
    double timestamp;

    /**
     * the constructor of BookingData Class
     * that will need for the Recycle view
     *
     * @param timestamp
     * @param valueOfBuching
     * @param ctategory
     */
    public BookingData(double timestamp, float valueOfBuching, String ctategory) {
        this.ctategory = ctategory;
        this.valueOfBuching = valueOfBuching;
        this.timestamp = timestamp;
    }

    public double getTimestamp() {return timestamp; }

    public void setTimestamp(double timstamp) {
        this.timestamp = timstamp;
    }

    public String getCategory() {
        return ctategory;
    }

    public void setCategory(String category) {
        this.ctategory = category;
    }

    public float getValueOfBuching() {
        return valueOfBuching;
    }

    public void setValueOfBuching(float valueOfBuching) {
        this.valueOfBuching = valueOfBuching;
    }



}
